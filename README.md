# vecty pwa

Golang + Vecty = SPA + PWA で 疑似アプリを作るテスト

## 環境

* Windows 10 Pro
* Golang 1.13rc2
* gopherjs/vecty latest

## Webサーバの起動
`$ goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir(".")))'`
