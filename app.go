package main

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"github.com/gopherjs/vecty/prop"
	"strconv"
	"syscall/js"
	"time"
)

var (
	count         = 0
	initialRender = false
	v             = new(page)
	localStorage  = js.Global().Get("localStorage")
)

func init() {
	if data := localStorage.Get("count"); data != js.Undefined() {
		count, _ = strconv.Atoi(data.String())
	}
}

func countUp() {
	if initialRender {
		count++
		localStorage.Set("count", strconv.Itoa(count))
		vecty.Rerender(v)
		time.Sleep(3 * time.Second)
	}
	countUp()
}

func main() {
	vecty.SetTitle("Vecty PWA TEST")
	go countUp()
	vecty.RenderBody(v)
}

type page struct {
	vecty.Core
}

func (c *page) Render() vecty.ComponentOrHTML {
	initialRender = true
	return elem.Body(elem.Input(vecty.Markup(prop.Value(strconv.Itoa(count)))))
}
