const CACHE_NAME = "v0.0.1";
const CACHE_FILES = [
    "/",
    "/index.html",
    "/app.wasm",
    "/js/wasm_exec_1_13.js",
];

self.addEventListener('install', function (e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(caches.open(CACHE_NAME).then((cache) => {
        cache.addAll(CACHE_FILES)
    }));
});

self.addEventListener('fetch', function (e) {
    console.log('[ServiceWorker] Fetch');
    e.respondWith(caches.match(e.request).then((response) => {
        if (response) {
            return response
        }

        return fetch(e.request).then((res) => {
            return caches.open(CACHE_NAME).then((cache) => {
                cache.put(e.request.url, res.clone());
                return res;
            })
        })
    }));
});

self.addEventListener('activate', function (e) {
    console.log('[ServiceWorker] Activate');
});
